# Stack Ratio

Google Chrome extension for stackexchange.com sites displaying user answers to posts ratio.

![Screen](https://gitlab.com/Prologh/stack-ratio/-/raw/master/img/screens/screen-01.PNG)

Why? To avoid wasting time on low quality and low value questions from users that tend to abuse Stack Overflow platform with their problems caused in the majority of cases by lack of ability to debug, troubleshoot or even google for solutions despite 10+ years of commercial experience. An example:

![Screen](https://gitlab.com/Prologh/stack-ratio/-/raw/master/img/screens/screen-02.png)

And to highlight questions that very likely will be of high quality and present unusual problems. Users with high score and high ratio tend to post a question as a last resort, so helping them will certainly be rewarding. An example:

![Screen](https://gitlab.com/Prologh/stack-ratio/-/raw/master/img/screens/screen-03.png)


## How to install

1. Have a Stack Overflow account
2. Install the extension
3. Click on the extension
4. Click on _Allow API access_ button
5. Click on _Allow_ in the new window
6. Enjoy configured extension

## How to uninstall

Simply remove the extension from your browser.

To remove access granted to this app from your Stack Overflow account:

1. Go to [stackoverflow.com](https://stackoverflow.com/)
2. Click on your profile picture
3. Go to _Settings_ section
4. Click on _Authorized applications_ on the left navigation bar, at the very bottom
5. Click on _Remove_ button near Stack Ratio application entry

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/stack-ratio/-/raw/master/LICENSE).

## Privacy policy

This plugin does not store or share any sensitive data. Full policy available [here](https://justpaste.it/cabes).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).