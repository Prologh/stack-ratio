﻿const appId = 19118;
const appKey = 'A160hLFmr4y6lQu8AScs6Q((';
const siteNameConst = 'stackoverflow';
const apiUrl = 'https://api.stackexchange.com/2.2/users/{0}/{1}?site={2}&filter=total&access_token={3}&key={4}';

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    };
}

isApiConnected()
    .then(entry => {
        var token = entry['stackexchange_api_access_token'];
        var isConnected = !$.isEmptyObject(entry) && token;

        if (!isConnected){
            // TODO: Save the to the popup somehow?
            // Or show in the icon.
            console.log("API is not connected")
            return;
        }
        
        $('.user-details').each((i, userDetails) => {
            let userId = getUserId(userDetails);
            if (!(userId) || userId === "-1") {
                return;
            }
    
            getUserStats(userId, token)
                .then((userStats) => {
                    let ratioBadgeHtml = getUserRatioBadgeContent(userStats);
    
                    $(userDetails)
                        .children('a[href^="/users/' + userId + '"]')
                        .after(ratioBadgeHtml);
                });
        });

        $('.s-user-card--info').each((i, userCard) => {
            let userId = getUserId(userCard);
            if (!(userId) || userId === "-1") {
                return;
            }
    
            getUserStats(userId, token)
                .then((userStats) => {
                    let ratioBadgeHtml = getUserRatioBadgeContent(userStats);
    
                    $(userCard)
                        .children('ul.s-user-card--awards')
                        .after(ratioBadgeHtml);
                });
        });
    });

function isApiConnected() {
    return chrome.storage.sync.get('stackexchange_api_access_token');
}

function getUserId(userDetails) {
    try {
        return $(userDetails).find('a[href^="/users/"]').attr('href').split('/')[2];
    } catch { }
}

function getUserStatsStorageKey(userId){
    return 'stack_ration_user_stats_' + userId;
}

function getUserAnswersRatio(answers, questions) {
    return Math.round(answers / (answers + questions) * 100);
}

async function getUserStatsFromApi(userId, accessToken){
    const results = await Promise.all([
        getUserAnswersCountFromApi(userId, siteNameConst, accessToken),
        getUserQuestionsCountFromApi(userId, siteNameConst, accessToken)
    ]);

    return {
        answers: results[0].total,
        questions: results[1].total,
    };
}

function getUserStats(userId, accessToken){
    return getUserStatsFromStorage(userId)
        .then(entry => {
            if (!$.isEmptyObject(entry)){
                var key = getUserStatsStorageKey(userId);

                return entry[key];
            }

            return getUserStatsFromApi(userId, accessToken)
                .then(stats => setUserStats(userId, stats));
        });
}

function getUserStatsFromStorage(userId){
    var key = getUserStatsStorageKey(userId);

    return chrome.storage.sync.get(key);
}

function getUserAnswersCountFromApi(userId, siteName, accessToken) {
    var requestUrl = apiUrl.format(userId, 'answers', siteName, accessToken, appKey);

    return $.ajax({
        url: requestUrl,
        dataType: 'json'
    });
}

function getUserQuestionsCountFromApi(userId, siteName, accessToken) {
    var requestUrl = apiUrl.format(userId, 'questions', siteName, accessToken, appKey);
    
    return $.ajax({
        url: requestUrl,
        dataType: 'json'
    });
}

function getUserRatioBadgeContent(userStats) {
    let userRatio = getUserAnswersRatio(userStats.answers, userStats.questions);
    let backgroundColor = getBackgroundColorForRatio(userRatio, userStats.answers, userStats.questions);
    let foregroundColor = getForegroundColorForRatio(userStats.answers, userStats.questions);

    return ' <span class="user-ratio" style="'
        + 'background-color: ' + backgroundColor + '; border-radius: 4px; padding: 2px 4px; font-size: .8rem; '
        + 'color: ' + foregroundColor +';">'
        + userRatio + '%</span>';
}

function getBackgroundColorForRatio(userRatio, answers, questions) {
    if (isUnexperiencedAccount(answers, questions))
        return '#7a7a7a';

    if (userRatio <= 25)
        return '#8b0000';
    else if (userRatio <= 50)
        return '#965300';
    else if (userRatio <= 75)
        return '#198006';
    else
        return '#500eb9';
}

function getForegroundColorForRatio(answers, questions) {
    if (isUnexperiencedAccount(answers, questions)){
        return '#ffffff';
    } else {
        '#dddddd';
    }
}

function isUnexperiencedAccount(answers, questions){
    const NewAccountTreshold = 10;

    return answers + questions <= NewAccountTreshold;
}

function setUserStats(userId, userStats){
    var key = getUserStatsStorageKey(userId);
    var entry = {};
    entry[key] = userStats;

    chrome.storage.sync.set(entry);

    return userStats;
}
