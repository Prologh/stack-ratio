const clientId = 19118;
const tokenScope = 'no_expiry';
const oAuthDialogUrl = 'https://stackoverflow.com/oauth/dialog?client_id={0}&scope={1}&redirect_uri={2}';

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    };
}

$('.oAuthDialog').on("click", () => {
    let redirectUrl = chrome.identity.getRedirectURL("stackexchange");
    let authUrl = oAuthDialogUrl.format(clientId, tokenScope, encodeURI(redirectUrl));

    chrome.identity.launchWebAuthFlow({
        'url': authUrl,
        'interactive': true
    },
    (redirectedTo) => {
        if (chrome.runtime.lastError) {
            // TODO: Log the error in the popup.
            console.log(chrome.runtime.lastError.message);
        }
        else {
            var accessToken = redirectedTo.split('access_token=')[1].split('&')[0];
            // TODO: Check if access token is not empty or null
            // TOOD: Clear the error message in the popup.
            chrome.storage.sync.set({'stackexchange_api_access_token': accessToken})
                .then(() => {
                    console.log("token set");
            });
        }  
    })
});
